# -*- coding: utf-8 -*-
import numpy as np
import random


class Network:

    def __init__(self):
        self.weights1 = []
        self.weights2 = []
        self.threshold1 = []
        self.activation_func = np.vectorize(lambda x: x if x > 0 else 0)

    def __str__(self):
        return str(self.weights1) + "\n" + str(self.threshold1) + "\n" + str(self.weights2)

    def train(self, vectors):
        """
        Обучает сеть
        :param vectors: набор образов для запоминания
        :return:
        """
        self.weights1 = np.array(vectors).T / 2

        self.weights2 = np.empty([len(vectors), len(vectors)])
        self.weights2.fill(-1/(2*len(vectors)))

        self.threshold1 = np.empty(len(vectors))
        self.threshold1.fill(len(vectors[0]) / 2)

        for idx in range(len(vectors)):
            self.weights2[idx][idx] = 1

    def recognize(self, vector):
        """
        Распозает образ
        :param vector:
        :return:
        """
        inputs1 = np.array(vector)
        outputs1 = np.dot(inputs1, self.weights1) + self.threshold1

        outputs2 = self.activation_func(outputs1)
        prev_output = outputs2

        while np.count_nonzero(outputs2) > 1:
            prev_output = outputs2
            outputs2 = self.activation_func(np.dot(outputs2, self.weights2))
            print(outputs2)

        winner = np.nonzero(outputs2)[0].tolist()

        if len(winner) < 1:
            winners = np.nonzero(prev_output)[0].tolist()
            win_idx = random.randrange(len(winners))
            winner = winners[win_idx]
        else:
            winner = winner[0]

        print("Winner: " + str(winner))

        rec_vec = np.transpose(self.weights1)[winner] * 2

        return rec_vec.tolist()
