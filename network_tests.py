import network as nw
import csv_procesor as csv_p


def test1():
    network = nw.Network()
    network.train(csv_p.get_vector_list("resources/test1mem.csv"))

    vectors = csv_p.get_vector_list("resources/test1rec.csv")
    for vector in vectors:
        print("Input:\n" + str(vector) + "\nHopfield network output:")
        print([int(x) for x in network.recognize(vector)])
        input()


def test2():
    network = nw.Network()
    network.train(csv_p.get_vector_list("resources/test2mem.csv"))

    vectors = csv_p.get_vector_list("resources/test2rec.csv")
    for vector in vectors:
        print("Input:\n" + str(vector) + "\nHopfield network output:")
        print([int(x) for x in network.recognize(vector)])
        input()

