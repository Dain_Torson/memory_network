# -*- coding: utf-8 -*-
import csv


def get_vector_list(filename):
    """
    Получает список векторов из csv файла
    :param filename:
    :return:
    """
    f = open(filename, newline='')
    reader = csv.reader(f, delimiter=';')
    vectors = []

    for row in reader:
        vectors.append(list(map(int, row)))

    return vectors
